import React, { Component } from 'react'

export default class FlexLayout extends Component {
    constructor(props){
        super(props);
        this.state={
            id:0,
            name:'',
            age:0,
            school:'',
            skill:'',
        };
    }
    updateInfor=() =>{
        this.setState({
            id:1,
            name:'Kanha',
            age:88,
            school:'RUPP',
            skill:'IT',
        });
    }
  render() {
    return (
      <header>
        <div className='container display-flex justify-center flex flex-wrap  mx-auto'>
            <div className="full w-full p-5 text-3xl font-bold underline text-blue-800">Information Student</div>
            <div className='item w-1/8 bg-purple-600 p-20 ml-2 text-3xl'>
                {this.state.id}
            </div>
            <div className='item w-1/8 bg-purple-600 p-20 ml-2 text-3xl'>
                {this.state.name}
            </div>
            <div className='item w-1/8 bg-purple-600 p-20 ml-2 text-3xl'>
                {this.state.age}
            </div>
            <div className='item w-1/8 bg-purple-600 p-20 ml-2 text-3xl'>
                {this.state.school}
            </div>
            <div className='item w-1/8 bg-purple-600 p-20 ml-2 text-3xl'>
                {this.state.skill}
            </div>
        </div>
        <button onClick={()=> this.updateInfor()} className="bg-red-600 rounded-md p-2 mt-2 text-white font-bold pr-7 pl-7">Click Me</button>
      </header>
    )
  }
}
