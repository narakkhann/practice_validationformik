import React, { Component } from 'react'

export default class TableComponent extends Component {
  render() {
    return (
      <div>
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-24 ml-36">
          {/* Heading */}
          <h2 className='font-bold text-4xl mb-5'>Dispaly Student Name</h2>
          {/* Table Dispaly */}
          <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
              <tr>
                <th scope="col" class="px-6 py-3">
                  ID
                </th>
                <th scope="col" class="px-6 py-3">
                  Student Name
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.data.map((item) => (
                <tr key={item.id} class="border-b bg-gray-50 dark:bg-gray-800 dark:border-gray-700">
                  <td class="px-6 py-4">{item.id}</td>
                  <td class="px-6 py-4">{item.stuName}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}
