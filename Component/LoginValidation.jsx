import React, { Component } from 'react'
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
const SignupSchema = Yup.object().shape({
    password: Yup.string()
        .required("Password is required")
        .matches(
            /^((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,})$/g,
            "Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"),
    email: Yup.string()
       .email('Invalid email')
       .required('Email is equired'),
});
export default class LoginValidation extends Component {
  render() {
    return (
        <div>
            <Formik
                initialValues={{
                    firstName: '',
                    lastName: '',
                    email: '',
                }}
                validationSchema={SignupSchema}
                onSubmit={values => {
                    // same shape as initial values
                    console.log(values);
                }}
            >
                {({errors, touched }) => (
                    <Form className='w-1/4 mx-auto p-3 bg-blue-300 rounded-md text-black mt-5 pb-10'>
                        <h1 className='font-semibold text-2xl mb-2 mt-0 text-white'>Sign Up</h1>
                        <Field name="email" type="email" className="border-1 border-black mb-2 w-full mb-2 p-2 rounded-lg"placeholder="Email"/>
                        {errors.email && touched.email ? <div className='text-red-600 font-semibold'>{errors.email}</div> : null}
                        <br />
                        <Field name="password" className="border-1 border-black w-full p-2 rounded-lg mt-3 "placeholder="Password"/>
                        {errors.password && touched.password ? (
                            <div className='text-red-600 font-semibold'>{errors.password}</div>
                        ) : null} <br />
                        <button type="submit" className='bg-blue-700 rounded-md py-1 px-4 text-white mt-10 hover:bg-blue-800 font-semibold'>Submit</button> <br />
                    </Form>
                )}
            </Formik>
      </div>
    )
  }
}