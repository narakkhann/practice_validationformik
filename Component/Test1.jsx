import React,{ Component } from 'react';
export default class Test1 extends Component {
    constructor(){
        super();
        this.state={
            count:0,
        };
    }
    updateCount =()=>{
        this.setState((prev)=>{
            return {count : prev.count+1}
        })
    }
    render() {
    return (
      <div>
        <h1 class='text-3xl text-red-800 font-bold'>Count Timer</h1>
        <button onClick={this.updateCount} > Click{this.state.count} time</button>
      </div>
    )
  }
}
