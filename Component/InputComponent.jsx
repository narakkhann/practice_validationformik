import React, { Component } from "react";
import TableComponent from "./TableComponent";

export default class InputComponent extends Component {
  constructor() {
    super();
    this.state = {
      student: [
        { id: 1, stuName: "Mann" },
        { id: 2, stuName: "Heang" },
        { id: 3, stuName: "Narak" },
      ],
      newStu: "",
    };
  }

  //   get Value from user input
  handleChange = (event) => {
    this.setState({
      newStu: event.target.value,
    });
  };

  //   Add new object to array

  handleSubmit = () => {
    const newObj = {
      id: this.state.student.length + 1,
      stuName: this.state.newStu,
    };
    this.setState(
      {
        student: [...this.state.student, newObj],
        newStu: "",
      },
      () => console.log(this.state.student)
    );
  };

  render() {
    return (
      <div className="w-2/4">
          
        {/* Form Input Student Name */}
        <div class="mt-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            for="username"
          >
            Input Student Name
          </label>
          <input
            className="shadow appearance-none border rounded w-2/4 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="username"
            type="text"
            placeholder="Student Name"
            onChange={this.handleChange}
          />
        </div>

        {/* Button Submit */}
        <button
          onClick={this.handleSubmit}
          class="bg-blue-500 mt-4 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
        >
          Send
        </button>

        {/* Table for Disaply ID and Student Name */}
        <TableComponent data = {this.state.student}/>

      </div>
    );
  }
}
